<?php

/**
 * @file
 *  Display content for upcoming block.
 *
 *  Available variables (mimic data returned from LibCal):
 *  
 *  - $library_id: Library ID (text string)
 *  - $lid: Library Location ID (libcal)
 *  - $name: Location Name
 *  - $category: Location category (e.g. department)
 *  - $desc: Long description for this Location
 *  - $url: URL provided by maintainers
 *  - $contact: Location maintainer contact
 *  - $lat: Geographic Latitude
 *  - $long: Geographic Longitude
 *  - $color: Display text color (will likely not use)
 *  - $parent_lid: Parent Location ID
 *  - $dates (array): Array with the dates returned from LibCal API call.
 *  - $public_dates (array): Array with the dates returned from LibCal API call (for general public hours).
 */
?>

<?php
  // how many days are we displaying?
  $num_days = count(array_keys($dates));
  $i = 0;
  
  // RDFa
  echo '<div vocab="https://schema.org/" typeof="Library">';
  echo '<meta property="name" content="' . $name . '"/>';

  while ( $i < $num_days ) {

  // when is today?

    $datesTemp = array_keys($dates); // fix for php 5.3
    //if ( array_keys($dates)[$i] == date("Y-m-d") ) {
    if ( $datesTemp[$i] == date("Y-m-d") ) { // fix for php 5.3
      $today_class="hours-today";
    } else {
      $today_class = "";
    }

    echo '<div class="hours-row ' . $today_class . '">';

      // echo '<span class="day">' . date('D n/j', strtotime(array_keys($dates)[$i])) . '</span>';
      echo '<span class="day">' . date('D n/j', strtotime($datesTemp[$i])) . '</span>'; // fix for php 5.3
      
      // $date = $dates[array_keys($dates)[$i]];
      $date = $dates[$datesTemp[$i]]; // fix for php 5.3

// dpm($dates);
// dpm($date);

    // are we open?
      if ( $date['status'] == 'closed' ) {

        echo '<span class="hours">CLOSED</span>';

      } elseif ( $date['status'] == '24hours' ) {

        echo '<span class="hours">Open 24 Hours</span>';
      
      } else {

        echo '<span class="hours">';
          echo '<span class="from">' . date('ga', strtotime($date['hours'][0]['from'])) . '</span>';
          echo '&ndash;';
          echo '<span class="to">' . date('ga', strtotime($date['hours'][0]['to'])) . '</span>';
        echo '</span>';

        // RDFa
        echo '<meta property="openingHours" content="' . date('D', strtotime($datesTemp[$i])) . ' ' . date('G', strtotime($date['hours'][0]['from'])) . '-' . date('G', strtotime($date['hours'][0]['to'])) . '"/>';

        // do we worry about secondary hours?

      }

      // public hours display
      if (variable_get('upcoming_days_display_public_'.$library_id) == 1 ) {

          $publicDatesTemp = array_keys($public_dates); // fix for php 5.3

          //$public_date = $public_dates[array_keys($public_dates)[$i]];
          $public_date = $public_dates[$publicDatesTemp[$i]]; // fix for php 5.3

// dpm($public_dates);
// dpm($public_date);
          
          if ( $public_date != '') {

            // is public open?
            if ( $public_date['status'] == 'closed' ) {

              echo '<div class="genpublic"><a href="' . variable_get('dul_hours_public_hours_news_url') . '">Public</a>: CLOSED</div>';

            } elseif ( $public_date['status'] == '24hours' ) {

              echo '<div class="genpublic"><a href="' . variable_get('dul_hours_public_hours_news_url') . '">Public</a>: 24 Hours</div>';
            
            } else {
              
              echo '<div class="genpublic"><a href="' . variable_get('dul_hours_public_hours_news_url') . '">Public</a>:&nbsp;';
                echo '<span class="public-hours">';
                  echo '<span class="from">' . date('ga', strtotime($public_date['hours'][0]['from'])) . '</span>';
                  echo '&ndash;';
                  echo '<span class="to">' . date('ga', strtotime($public_date['hours'][0]['to'])) . '</span>';
                echo '</span>';
              echo '</div>';

            }

          } else {
            echo '<div class="genpublic"><a href="' . variable_get('dul_hours_public_hours_news_url') . '">Public</a>:&nbsp;';
              echo '<span class="public-hours"><em>unavailable</em></span>';
            echo '</div>';
          }

        }

    echo '</div>';

    $i++;

  }

  echo '<div class="other">';
    echo '<a href="' . variable_get('upcoming_days_bottomlink_url_' . $library_id) . '">';
      echo variable_get('upcoming_days_bottomlink_title_' . $library_id);
    echo '</a>';
  echo '&nbsp;»</div>';


  echo '</div>';

?>

<?php // dpm(get_defined_vars()); // super variable dump!?>
