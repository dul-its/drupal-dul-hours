<?php

/**
 * Implements hook_schema()
 */
function dul_hours_schema() {
	$schema = array();

	$schema['duke_cal_library'] = array(
		'description' => 'Stores library information, including id, name and Google Calendar Id',
		'fields' => array(
			'lid' => array(
				'type' => 'serial',
				'unsigned' => TRUE,
				'not null' => TRUE,
			),
			'library_id' => array(
				'type' => 'varchar',
				'length' => 64,
				'not null' => TRUE,
				'default' => '',
			),
			'library_name' => array(
				'type' => 'varchar',
				'length' => 255,
				'not null' => TRUE,
				'default' => '',
			),
			'libcal_id' => array(
				'type' => 'varchar',
				'length' => 128,
				'not null' => TRUE,
				'default' => '',
			),
      'libcal_parent_id' => array(
				'type' => 'varchar',
				'length' => 128,
				'not null' => TRUE,
				'default' => '',
			),
      'library_order' => array(
				'type' => 'int',
        'not null' => TRUE,
				'unsigned' => TRUE,
				'default' => 0,
			),
			'last_calendar_fetch' => array(
				'type' => 'int',
				'not null' => TRUE,
				'unsigned' => TRUE,
				'default' => 0,
			),
		),
		'primary key' => array('lid'),
		'indexes' => array(
			'library_id' => array(array('library_id', 5)),
		),
	);

	$schema['hours'] = array(
		'description' => 'Stores calendar date entries',
		'fields' => array(
			'lid' => array(
				'type' => 'int',
				'not null' => TRUE,
				'unsigned' => TRUE,
				'default' => 0,
			),
			'start_date' => array(
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'end_date' => array(
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'event_title' => array(
				'type' => 'varchar',
				'length' => 128,
				'not null' => TRUE,
				'default' => '',
			),
			'event_data' => array(
				'type' => 'text',
				'size' => 'big',
			),
		),
		'foreign keys' => array(
			'library' => array(
				'table' => 'duke_cal_library',
				'columns' => array('lid' => 'lid'),
			),
		),
		'primary key' => array('lid', 'start_date'),
	);

	return $schema;
}

function dul_hours_install() {
	$o = new stdClass();
	$o->libcal_id = '11088';
  $o->libcal_parent_id = '11088';
  $o->library_order = '1';
	$o->library_id = 'perkins';
	$o->library_name = 'Perkins/Bostock Libraries';
	drupal_write_record('duke_cal_library', $o);

	$o = new stdClass();
	$o->libcal_id = '13162';
  $o->libcal_parent_id = '11088';
  $o->library_order = '2';
	$o->library_id = 'public';
	$o->library_name = t('General Public Hours');
	drupal_write_record('duke_cal_library', $o);

	$o = new stdClass();
	$o->libcal_id = '13161';
  $o->libcal_parent_id = '13161';
  $o->library_order = '3';
	$o->library_id = 'rubenstein';
	$o->library_name = t('David M. Rubenstein & Manuscript Library');
	drupal_write_record('duke_cal_library', $o);

	$o = new stdClass();
	$o->libcal_id = '11063';
  $o->libcal_parent_id = '11063';
  $o->library_order = '4';
	$o->library_id = 'lilly';
	$o->library_name = t('Lilly Library');
	drupal_write_record('duke_cal_library', $o);

  $o = new stdClass();
	$o->libcal_id = '17439';
  $o->libcal_parent_id = '11063';
  $o->library_order = '5';
	$o->library_id = 'lilly-public';
	$o->library_name = t('Lilly Public Hours');
	drupal_write_record('duke_cal_library', $o);

	$o = new stdClass();
	$o->libcal_id = '11150';
  $o->libcal_parent_id = '11150';
  $o->library_order = '6';
	$o->library_id = 'music';
	$o->library_name = t('Music Library');
	drupal_write_record('duke_cal_library', $o);

  $o = new stdClass();
	$o->libcal_id = '14354';
  $o->libcal_parent_id = '14354';
  $o->library_order = '7';
	$o->library_id = 'exhibits';
	$o->library_name = t('Exhibits Suite');
	drupal_write_record('duke_cal_library', $o);

  $o = new stdClass();
	$o->libcal_id = '17335';
  $o->libcal_parent_id = '17335';
  $o->library_order = '8';
	$o->library_id = 'after-hours';
	$o->library_name = t('After Hours Study Spaces');
	drupal_write_record('duke_cal_library', $o);

  variable_set('dul_hours.upcoming_days_block', array('exhibits', 'lilly', 'music', 'perkins', 'rubenstein'));
}

/** 
 * Implements hook_uninstall()
 */
function dul_hours_uninstall() {
	// unset variables
	variable_del('upcoming_days_count');
	variable_del('upcoming_days_start_date');
	variable_del('dul_hours.libcal_client_id');
	variable_del('dul_hours.libcal_client_secret');
	variable_del('dul_hours.default_library');
	variable_del('dul_hours.default_num_events');
}
