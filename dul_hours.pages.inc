<?php

// dpm(get_defined_vars()); // super variable dump!

/**
 * Display the hours for all library locations in one
 * table grid
 */
function dul_hours_table_page($form, &$form_state) {

  // hack to get about/hours page title to display correctly
  drupal_set_title('Library Hours');

	# RT 261619
	# WebX request to add links to the professional school libraries
	# @see affiliate_hours.tpl.php for default theme implementation
	#
	# set the weight to 100 to force to the bottom.
	$form['affiliate_hours'] = array(
		'#theme' => 'affiliate_hours',
		'#weight' => 100
	);
		
	# include the interface to the Libcal API
	require_once drupal_get_path('module', 'dul_hours') . '/dul_hours.calendar.inc';
	
	// ROADMAP:
	// 1) Retrieve the library ids (perkins, public, etc)
	$libraries = dul_hours_load_libraries();
	$access_token = dul_hours_load_access_token();

	// Determine closest "previous" Sunday and then the "upcoming" Saturday
	date_default_timezone_set('America/New_York');
	$now = time();

	$date_header = FALSE;


	// check for valid dates
	function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
	}

	// before we process the form,
	// check for start time URL param
	$params = drupal_get_query_parameters();
	$start_param = $params['start'];

	if ($start_param != null) {

		// is this a valid date?
		if ( validateDate($start_param, 'Y-m-d') == TRUE ) {
			//turn start param back into date
			$start_time_date = strtotime($start_param);
			$form_state['starttime'] = $start_time_date;
			$now = $start_time_date; // use param as starting point
		} 

	} else {
		if (isset($form_state['starttime'])) {
			$now = $form_state['starttime'];
			#error_log("using 'now' from submitted form = " . $now);
			#error_log("**** " . strftime('%A<br />%b %e', $now));
		}
	}

	
	$date_now = new DateTime();
	$date_now->setTimestamp($now);
	$date_now->setTime(0, 0);

	#$delta_day = (int) date('w', $now);
	$delta_day = (int) date('w', $date_now->getTimestamp());
	$date_sunday = $date_now->sub( new DateInterval( sprintf("P%sD", $delta_day ) ) );
	$date_sunday_header = $date_sunday;

	$sunday_time = $now - (86400 * $delta_day);
	$sunday_time = $date_sunday->getTimestamp();

	// we need to make sure Sunday starts at Sunday -- midnight
	$getdate = getdate( $sunday_time );
	$sunday_time = mktime(0, 0, 0, $getdate['mon'], $getdate['mday'], $getdate['year']);
	$sunday_time = $date_sunday->getTimestamp();

	$saturday_time = $now + ((86400 * (7 - $delta_day)) - 1); // seven days in a week, yes?
	$page_subtitle = strftime('%A, %B&nbsp;%e', $sunday_time) . ', ' . strftime('%A, %B&nbsp;%e', $saturday_time);
	
	$header = array('');
	for ($i = 0; $i < 7; $i++) {
		$header[] = strftime('%A, %B&nbsp;%e', $date_sunday_header->getTimestamp() );
		$date_sunday_header->add( new DateInterval('P1D') );
		#$header[] = strftime('%A<br />%b %e', $sunday_time + (86400 * $i));
	}
	
	$rows = array();
	
	// 2) For each, fetch a week's worth of data
	//    -- determine closest "previous" Sunday and upcoming "Saturday"
	foreach($libraries as $library) {

		$sunday_time_formatted = date('Y-m-d', $sunday_time);
		$saturday_time_formatted = date('Y-m-d', strtotime("+6 day", $sunday_time));

		$calevents = array();

		$hours_json = dul_hours_fetch_libcal_hours($library->libcal_id, $access_token, $sunday_time_formatted, $saturday_time_formatted);

		$hours_array = json_decode($hours_json, true);

		// print_r($hours_array);

			// renders library name in 1st column
			# $row['data'] = array( $library->library_name . '<div>' . l(t('View by month'), 'about/hours/' . $library->library_id));
      $row['data'] = array( l($library->library_name, 'about/hours/' . $library->library_id, array('attributes' => array('title' => 'View hours by month')) ) );
      
			// need to return if json is empty
			if (empty($hours_array)) {
				return;
			}

			// renders hours in subsequent columns

			// get formatted date strings to use in pulling out from libcal json
			// loop through each date starting with sunday
			// based on $sunday_time so updates with date range changes
			$week_of_dates_array = array($sunday_time_formatted);
			$i = 1;
			while ($i < 7) {
				array_push($week_of_dates_array, date('Y-m-d',strtotime("+$i day", $sunday_time)));
				$i++;
			}

			// loop through each date from above and display 'closed' status or hours
			foreach($week_of_dates_array as $date) {
				
				if ($date == date('Y-m-d')) {
					$today = 'today';
				} else {
					$today = '';
				}

				$status = $hours_array[0]['dates'][$date]['status'];

				if ($status == 'closed') {
					$row['data'][] = array('data' => 'Closed', 'width' => '11.5%', 'class' => $today);
				} elseif ($status == '24hours') {
          $row['data'][] = array('data' => 'Open 24 Hours', 'width' => '11.5%', 'class' => $today);
        } elseif ($status == 'ByApp') {
          $row['data'][] = array('data' => 'By Appointment', 'width' => '11.5%', 'class' => $today);
        } else {

          $day_0_from = $hours_array[0]['dates'][$date]['hours'][0]['from'];
          $day_0_to = $hours_array[0]['dates'][$date]['hours'][0]['to'];
          $day_1_from = $hours_array[0]['dates'][$date]['hours'][1]['from'];
          $day_1_to = $hours_array[0]['dates'][$date]['hours'][1]['to'];

          if ($day_0_from == $day_0_to) {
            $day_hours = '<em>unavailable</em>';
          } else {
            $day_hours = date('ga', strtotime($day_0_from)) . " &ndash; " . date('ga', strtotime($day_0_to));
            // account for second set of hours
            if ($day_1_from != '') {
              $day_hours .= "<hr />" . date('ga', strtotime($day_1_from)) . " &ndash; " . date('ga', strtotime($day_1_to));
            }
          }
					$row['data'][] = array('data' => $day_hours, 'width' => '11.5%', 'class' => $today);
				}
			}


		$rows[] = $row;

		$library->calevents = $calevents;
		$library->weight = $weight[$library->library_id];
	}
	
	$form['navigation'] = array(
		'#type' => 'container',
		'#attributes' => array(
			'class' => array('hours-table-buttons'),
		)
	);
	
	#get the 'next' previous sunday
	$previous_sunday = new DateTime();
	$previous_sunday->setTimestamp($sunday_time);
	$previous_sunday->sub(new DateInterval('P7D'));
	
	$next_sunday = new DateTime();
	$next_sunday->setTimestamp($sunday_time);
	$next_sunday->add(new DateInterval('P7D'));

	$form['time_previous'] = array(
		'#type' => 'hidden',
		'#value' => $previous_sunday->getTimestamp(),
	);
	$form['time_next'] = array(
		'#type' => 'hidden',
		'#value' => $next_sunday->getTimestamp(),
	);
	$form['navigation']['prev'] = array(
		'#type' => 'submit',
		'#value' => t('<< Prev'),
		'#suffix' => '&nbsp;',
	);
	$form['navigation']['next'] = array(
		'#type' => 'submit',
		'#value' => t('Next >>'),
	);
	
	$form['hoursTable'] = array(
		'#theme' => 'table',
		'#header' => $header,
		'#rows' => $rows,
		'#attributes' => array(
			'class' => array('table'),
		),
	);


  $form['#attached']['js'] = array(
      drupal_get_path('module', 'dul_hours') . '/dul_hours.table.js',
  );

	$form['#submit'][] = 'dul_hours_table_page_submit';
	return $form;


}

function dul_hours_table_page_submit($form, &$form_state) {
	if ($form_state['values']['op'] == 'Next >>') {

		drupal_goto('about/hours', array('query'=>array(
			'start'=>date('Y-m-d', $form_state['values']['time_next']),
		)));

		$form_state['starttime'] = $form_state['values']['time_next'];

		
	} else if ($form_state['values']['op'] == '<< Prev') {

		drupal_goto('about/hours', array('query'=>array(
			'start'=>date('Y-m-d', $form_state['values']['time_previous']),
		)));

		$form_state['starttime'] = $form_state['values']['time_previous'];

	}
	$form_state['rebuild'] = TRUE;
}


/**
 * Page callback for calender page 
 * that displays a Google calendar widget
 * @param stdClass $library - object containing information about the library
 * 
 */
function dul_hours_calendar_view($library) {
	// PREAMBLE: The $library object is already loaded from the database.
	// see the entry in dul_hours_menu for 'about/hours/%dul_hours_library'
	// the %dul_hours_library slug causes Drupal to look for an "autoload" function 
	// by the name of 'dul_hours_library_load' -- that function is in dul_hours.module.
	
	// STEP ONE -- create a "page" array containing elements that are 
	// to be 'rendered' by Drupal's rendering engine.
	// Best practice is to avoid returning HTML
	$page = array();
	
	// STEP TWO -- add an element to the page array that
	// will represent the libcal calendar widget
	// Use the #theme property/array key to specify the theme
	// to be used to render the element
	$page['calender-view'] = array(
		'#theme' => 'calendar-view',		// this theme needs to be defined in dul_hours_theme()
		'#libcal_id' => $library->libcal_id,
    '#libcal_parent_id' => $library->libcal_parent_id,
    '#library_id' => $library->library_id,
    );

    // Desired Javascript files are #attached to the page structure
    $page['#attached']['js'] = array(
        drupal_get_path('module', 'dul_hours') . '/dul_hours.pages.js',
    );
	
	// FINAL STEP -- return the page array back to the Drupal rendering engine.
	// Drupal will makes sure the appropriate template file or function is called.
	return $page;
}
