// This JS file will be used to 
// facilitate loading hours data for each available
// location (from DUL's LibCal) account

jQuery(document).ready(function($) {

  // improve hours table for a11y purposes

  $('#dul-hours-table-page table thead tr th').attr('scope','col');

  var attr = $('#dul-hours-table-page table tbody tr td').attr('width');
  
  $('#dul-hours-table-page table tbody tr td').each(function() {
    if (typeof $( this ).attr('width') == 'undefined' || attr == false) {
      $( this ).replaceWith('<th>' + $(this).html() + '</th>');
      $( this ).attr('scope','row')
    }
  });

  
});
