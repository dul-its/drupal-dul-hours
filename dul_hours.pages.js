// This JS file will be used to 
// facilitate loading hours data for each available
// location (from DUL's LibCal) account

(function($) {

  // generate output for libcal widget
  // via: https://duke.libcal.com/admin/hours/widgets

  Drupal.behaviors.dul_hours = {
    attach: function (context, settings) {
      $libcal_id = settings.dul_hours.libcal_id;
      $libcal_parent_id = settings.dul_hours.libcal_parent_id;
      $libcal_account_id = settings.dul_hours.libcal_account_id;

      var myCalendar = new $.LibCalHoursCal( $("#s_lc_mhw_" + $libcal_account_id + "_" + $libcal_parent_id), { iid: $libcal_account_id, lid: $libcal_parent_id, months: 6, systemTime: false, show_past: 1 });
    
    }
  };

})(jQuery);
